/***************************************************************************//**
 * @file    main.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    24.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Final Project - Sensor Dashboard
 * The project read all sensor data and send to desktop app through UART.
 * Clinet App with UI can be downloaded at https://gitlab.com/hp5588/sensor-dash-board-ui.git
 * LEDs and LCD can futher controlled by client app. 
 * LCD's info will be written to flash and shown at next reboot.
 * *
 ******************************************************************************/

#include "libs/templateEMP.h"   // UART disabled, see @note!
#include "libs/lcd.h"
#include "libs/flash.h"
#include "libs/adac.h"
#include "libs/adc.h"
#include "libs/joystick.h"
#include "libs/uart.h"
#include "libs/pwm.h"
#include "libs/US.h"
#include "libs/buttons.h"
#include "libs/mma.h"
#include "libs/server.h"
#include <string.h>
/**
 * 	P1.0 -> NTC 					            <NEW>
 *  P1.3 -> LDR						            <NEW>
 * 	P1.4 -> POT						            <NEW>
 * 
 *  P1.5 -> CC_CLK (SPI clock)
 *  P1.6 -> SCL (ADAC/MMA)
 *  P1.7 -> SDA (ADAC/MMA)
 * 
 *  P2.0 -> D4 or S0(2)
 *  P2.1 -> D5 or S1(2)
 *  P2.2 -> D6 or S0(1)				            <shared LCD, 74HC194>
 *  P2.3 -> D7 or S1(1)				            <shared LCD, 74HC194>
 * 	P2.4 -> Rx_COMP  or CK(1/2)	<NEW>			<shared UltraSonic RX, 74HC194>
 *  P2.5 -> x  or CLR                           <74HC194>
 *  P2.6 ->    or SR(2)                         <74HC194>
 *  P2.7 -> x  or QD(1)				            <74HC194>
 *  
 *  P3.0 -> RS 
 *  P3.1 -> RW 
 *  P3.2 -> E  
 *  reset JP2 on board 2 to provide constant backlight on LCD
 *  P3.3 -> PB5						            <NEW>
 *  P3.4 -> F_/CS
 * 	P3.5 -> I2C/SPI (74HCT4066)   	            <!!! NOTICE Change !!!>
 * 	P3.6 -> US_CLK (ultra sonic CLK)            <NEW>
 *  P3.7 -> PB6						            <NEW>
 *  
 * 
 *  F_/HOLD -> VCC					            <!!! NOTICE Change !!!>
 *  F_/WP -> VCC					            <!!! NOTICE Change !!!>
 *  CC_SO -> F_SO
 *  CC_SI -> F_SI
 *  CC_CLK-> F_CLK
 */

#define ADC_CH_POT ADC_CH4
#define ADC_CH_NTC ADC_CH0
#define ADC_CH_LDR ADC_CH3

#define I2C_ADAC_ADDR 0x48 // A0-A2 are grounded -> 7-bit address is 0b1001000
#define I2C_MMA_ADDR 0x1D

#define FLASH_DATA_ADDR (FLASH_SEC1_BGN_ADDR)

#define IS_I2C (P1OUT & BIT3)

// ISR function pointers
typedef void (*FuncPtr) (uint8_t dir); // 0: RX, 1:TX
static FuncPtr i2c_isr_ptr;
static FuncPtr spi_isr_ptr;
static FuncPtr uart_isr_ptr;
void register_i2c_ISR(FuncPtr func) {
    i2c_isr_ptr = func;
}
void register_spi_ISR(FuncPtr func) {
    spi_isr_ptr = func;
}
void register_uart_ISR(FuncPtr func) {
    uart_isr_ptr = func;
}


void _value_print(char* title, uint16_t value){
    uint8_t str[10];

    uint16_t level = adc_read_channel(ADC_CH_LDR);
    serialPrint(title);
    sprintf(str, ": %d", value);
    serialPrintln(str);

}



int main(void)
{
	initMSP();

	register_uart_ISR(&UART_USCI0_ISR);
    register_i2c_ISR(&I2C_USCIAB0_ISR);
    register_spi_ISR(&SPI_USCIAB0_ISR);

    i2c_init(I2C_ADAC_ADDR);
    adac_init();

    i2c_init(I2C_MMA_ADDR);
    mma_init();
    mma_setRange(2);


    adc_init();
	buttons_init();
    led_init();
	us_init();
	lcd_init();
    uart_init();


    uint8_t buffer[16];
    spi_init();
	flash_init();
    flash_read(FLASH_DATA_ADDR, sizeof(buffer), buffer);

    lcd_putText("last msg:");
    lcd_cursorSet(0,1);
    lcd_putText(buffer);

    serialPrintln("start");

    

	while (1)
	{
        
        uint8_t adac_values[4];
        i2c_init(I2C_ADAC_ADDR);
        adac_read(&adac_values);

        i2c_init(I2C_MMA_ADDR);
        mma_read();

        uint8_t buttons_status = buttons_read();

	    uint16_t pot_level = adc_read_channel(ADC_CH_POT);
        uint16_t ntc_level = adc_read_channel(ADC_CH_NTC);
        uint16_t ldr_level = adc_read_channel(ADC_CH_LDR);

        uint16_t mma_x = mma_get14X();
        uint16_t mma_y = mma_get14Y();
        uint16_t mma_z = mma_get14Z();

        uint8_t us_distance = (uint8_t)((int)us_get_distance());

        server_clear();
        server_append(SRV_TYPE_POT, (uint8_t*)&pot_level);
        server_append(SRV_TYPE_LDR, (uint8_t*)&ldr_level);
        server_append(SRV_TYPE_NTC, (uint8_t*)&ntc_level);

        server_append(SRV_TYPE_MMA_X, (uint8_t*)&mma_x);
        server_append(SRV_TYPE_MMA_Y, (uint8_t*)&mma_y);
        server_append(SRV_TYPE_MMA_Z, (uint8_t*)&mma_z);

        server_append(SRV_TYPE_JS_X, &adac_values[1]);
        server_append(SRV_TYPE_JS_Y, &adac_values[2]);
        server_append(SRV_TYPE_JS_B, &adac_values[3]);

        server_append(SRV_TYPE_US, &us_distance);
        server_append(SRV_TYPE_BUTTONS, &buttons_status);


        server_update();

		std_delay(1000); // 1 s
	}
	
	
	return 0;
}

#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCIAB0TX_ISR(void)
{   
    // exclusive usage of B0 module, either as I2C or SPI
    // indivisual flag should be handled in isr handler of lib
    if (IS_I2C){
        i2c_isr_ptr(1);
    }else{
        spi_isr_ptr(1);
    }

    // UART TX
    if (UCA0TXIFG & IFG2){
        uart_isr_ptr(1);
    }


}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_ISR(void)
{
    if (IS_I2C){
        // i2c RX
        i2c_isr_ptr(0);
    }else{
        // SPI
        spi_isr_ptr(0);
    }
    

    // UART RX
    if (UCA0RXIFG & IFG2){
        uart_isr_ptr(0);
    }

}
