/***************************************************************************//**
 * @file    i2c.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    24.05.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 2 - I2C
 *
 * the basic operation read/write of I2C protocol
 ******************************************************************************/

#ifndef EXERCISE_LIBS_I2C_H_
#define EXERCISE_LIBS_I2C_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/******************************************************************************
 * CONSTANTS
 *****************************************************************************/
#define SDA    BIT7 // P1.7
#define SCL    BIT6 // P1.6
#define I2C_EN BIT5 // P3.5

#define LCD_MAX_LINE_CHARS 16

typedef enum{
    STOP,
    READ,
    WRITE
} Status;

/******************************************************************************
 * VARIABLES
 *****************************************************************************/
void i2c_enter_stop_condition();



/******************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/

// Initialize the I2C state machine. The speed should be 100 kBit/s.
// <addr> is the 7-bit address of the slave (MSB shall always be 0, i.e. "right alignment"). (2 pts.)
void i2c_init (unsigned char addr);

// Write a sequence of <length> characters from the pointer <txData>.
// Return 0 if the sequence was acknowledged, 1 if not. Also stop transmitting further bytes upon a missing acknowledge.
// Only send a stop condition if <stop> is not 0. (2 pts.)
unsigned char i2c_write(unsigned char length, unsigned char * txData, unsigned char stop);

// Returns the next <length> characters from the I2C interface. (2 pts.)
void i2c_read(unsigned char length, unsigned char * rxData);



// ISR
void I2C_USCIAB0_ISR(uint8_t dir);

#endif /* EXERCISE_LIBS_I2C_H_ */
