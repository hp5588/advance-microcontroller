/*
 * buttons.c
 *
 *  Created on: 2019�~6��20��
 *      Author: hp5588
 */
#include "./buttons.h"

#define CLK_HIGH P2OUT |= BIT4; // clk LOW
#define CLK_LOW  P2OUT &= ~BIT4; // clk LOW

#define SERIAL_MODE  P2OUT |= BIT2; P2OUT &= ~BIT3;
#define PARALLEL_MODE   P2OUT |= BIT2 + BIT3; // parellel load mode

#define CLR_HOLD P2OUT &= ~BIT5;
#define CLR_RELEASE P2OUT |= BIT5;


#define GET_P2_4  P2DIR|=BIT4;  P2SEL &= ~BIT4;   P2SEL2 &= ~BIT4;

void _latch_data(){
    CLK_LOW
    buttons_latch_delay();
    CLK_HIGH
}

void _clear_load(){
    CLK_LOW
    // CLR_HOLD
    P2OUT &= ~BIT5;
    buttons_clear_delay();
    P2OUT |= BIT5;
    // CLR_RELEASE
}

void buttons_init(){
    // setup button PB5 (P3.3), PB6(P3.7)
    P3DIR &= ~ (BIT3 + BIT7);
    P3REN |= (BIT3 + BIT7);
    P3OUT |= (BIT3 + BIT7);

    // S0(P2.2), S1(P2.3), CK(P2.4), CLR(P2.5), QD(P2.7)
    P2DIR |= BIT2 + BIT3 + BIT4 + BIT5; // S0, S1, CK, CLR as output
    P2DIR &= ~BIT7; // QD as input

    P2SEL &= ~BIT7;
    P2SEL2 &= ~BIT7;

    // setup 74HC194 shift register
    PARALLEL_MODE // parellel load mode
    _clear_load();
}

uint8_t buttons_read(){
    GET_P2_4
    uint8_t out = 0x00;

    // read PB5, PB6
    out |= (P3IN & BIT3) << 1;
    out |= (P3IN & BIT7) >> 2;

    // parallel mode
    PARALLEL_MODE
    CLR_RELEASE
    // latch data
    _latch_data();
    
    // serial mode (shift right)
    SERIAL_MODE
    
    // shift data with clk    
    uint8_t i=0;
    for (; i < 4; i++)
    {
        // std_delay_1ms();
        // std_delay(100);
        out |= ((P2IN & BIT7) >> (4 + i));
        _latch_data();
    }

    return out;
    
}



