/*
 * adc.h
 *
 *  Created on: 2019�~6��20��
 *      Author: hp5588
 */

#ifndef LIBS_ADC_H_
#define LIBS_ADC_H_
#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "./uart.h" // TODO: remove later

#define ADC_CH0 (0*0x01)
#define ADC_CH1 (1*0x01)
#define ADC_CH2 (2*0x01)
#define ADC_CH3 (3*0x01)
#define ADC_CH4 (4*0x01)
#define ADC_CH5 (5*0x01)
#define ADC_CH6 (6*0x01)
#define ADC_CH7 (7*0x01)


void adc_init();
uint16_t adc_read_channel(uint8_t channel);


#endif /* LIBS_ADC_H_ */
