/***************************************************************************//**
 * @file    spi.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    16.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 4 - SPI
 * Here goes a detailed description if required.
 ******************************************************************************/

#ifndef LIBS_SPI_H_
#define LIBS_SPI_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/******************************************************************************
 * CONSTANTS
 *****************************************************************************/
#define CS_HIGH P3OUT |= BIT4 //P3.4
#define CS_LOW P3OUT &= ~BIT4 //P3.4

#define HOLD_HIGH P3OUT |= BIT3 //P3.3
#define HOLD_LOW P3OUT &= ~BIT3 //P3.3


/******************************************************************************
 * VARIABLES
 *****************************************************************************/

/******************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/

// Set the USCI-machine to SPI and switch the 74HCT4066 (1 pt.)
void spi_init(void);

// Read <length> bytes into <rxData> (1 pt.)
void spi_read(unsigned char length, unsigned char * rxData);

unsigned int spi_read_byte();


// Write <length> bytes from <txData> (1 pt.)
void spi_write(unsigned char length, unsigned char * txData);

// Interrupt service routines in your spi.c (1 pt.)

// Returns 1 if the SPI is still busy or 0 if not.
// Note: this is optional. You will most likely need this, but you don't have
// to implement or use this.
unsigned char spi_busy(void);



// ISR
void SPI_USCIAB0_ISR(uint8_t dir);

#endif /* LIBS_SPI_H_ */
