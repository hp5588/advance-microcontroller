/*
 * US.c
 *
 *  Created on: 2019�~6��20��
 *      Author: hp5588
 */
#include "./US.h"
#define GET_P2_4   P2DIR &= ~BIT4;  P2SEL |= BIT4;   P2SEL2 &= ~(BIT4);


static bool _FLAG_RECEIVED_RSP = false;
static bool _FLAG_TIMEOUT = false;

static uint16_t captured_value = 0;

void _us_start_capture(){
    TA1CCTL2 = CM_2 + CCIS_0 + SCS + CAP + CCIE;
    TA1CTL = MC_1 + TASSEL1 + ID0 + ID1 + TACLR + TAIE;
}


void _us_stop_capture(){
    TA1CTL = MC0 + TACLR;
    TA1CCTL2 = CM_0 + CCIS_0 + SCS + CAP + CCIE;
}

void us_init(){
    // [Timer1_A3 CCI2B] setup signal capture timer 
    // P2DIR &= ~BIT5;
    // P2SEL |= BIT5;
    // P2SEL2 &= ~(BIT5);
    GET_P2_4

    // range up to 4m ( 11.76 ms * 2 ) -> set as timeout
    TA1CCR0 = 0x5BE0;

    // falling edge, CCI2B, sync, capture mode, en INT
    TA1CCTL2 = CM_2 + CCIS_0 + SCS + CAP + CCIE + TAIE; 


    // halt (count-up), SMCLK, clear status, interrupt enable
    TA1CTL = MC_0 + TASSEL1 + ID0 + ID1 + TACLR; 
}


float us_get_distance(){
    GET_P2_4

    _FLAG_RECEIVED_RSP = false;
    _FLAG_TIMEOUT = false;
    
    // init the pwm for ultra sonic clock and capture module
    pwm_init();
    _us_start_capture();
        
    __delay_cycles(8 * 294); // 10 cm (294 us)
    pwm_stop();
    while (!_FLAG_RECEIVED_RSP & !_FLAG_TIMEOUT);

    _us_stop_capture();

    // interpret value
    // 1 us / count
    // 0.034 cm / 1 us
    // single path distance -> 0.034/2 = 0.017
    return captured_value * 0.017;
}



// process capture flag
#pragma vector = TIMER1_A1_VECTOR
__interrupt void TIMER1_A1_ISR(void){
	if (TA1CCTL2 & CCIFG){
        _FLAG_RECEIVED_RSP = true;
        captured_value = TA1CCR2;
        TA1CCTL2 &= ~CCIFG; // clear flag
    }
    if (TA1CTL & TAIFG){
        // timeout interrupt
        _FLAG_TIMEOUT = true;
        captured_value = 0;
        TA1CTL &= ~TAIFG; // clear flag
    }
}

