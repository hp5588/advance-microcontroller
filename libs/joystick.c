/*
 * @file    joystock.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    16.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 4 - SPI
 */
#include "./joystick.h"
static unsigned int _xValue = 0; 
static unsigned int _yValue = 0;

static unsigned int _ragne = 40;

void js_calibrate(unsigned int xValue, unsigned int yValue){
    _xValue = (_xValue == 0) ? xValue : (xValue + _xValue)/2;
    _yValue = (_yValue == 0) ? yValue : (yValue + _yValue)/2;;
}

JsAction js_convert_action(unsigned int xValue, unsigned int yValue, unsigned int bValue){
    
    if (bValue > 0XF0){
        return JS_PRESS;
    } 
    
    if(abs(xValue - _xValue) > _ragne){
        return xValue > _xValue ? JS_LEFT : JS_RIGHT;
    } 
    if(abs(yValue - _yValue) > _ragne){
        return yValue > _yValue ? JS_DOWN : JS_UP;
    }

    return JS_NONE;
}


void js_set_neutral_range(unsigned int range){
    _ragne = range;
}
