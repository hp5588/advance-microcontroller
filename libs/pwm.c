/*
 * pwm.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    24.05.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 2 - I2C
 */
#include "./pwm.h"

//static uint8_t baseFreq = 100; // Hz
// #define base_reg_value (0x4E20 >> 4) // Hz
// #define step (0x44 >> 4) // (base_reg_value - base_reg_value/8)  / 256

#define base_reg_value (0x00C8) // 4kHz
#define step (0x01) // (base_reg_value - base_reg_value/8)  / 256

// #define base_reg_value (0x0019) // 4kHz
// #define step (0x01) // (base_reg_value - base_reg_value/8)  / 256
#define dutycycle_step (current_base_value / 100)

static uint16_t current_base_value = base_reg_value;
static uint8_t dutycycle = 50;

void pwm_init(){
    // P3.6 map pin to output signal from T0.2
    P3DIR |= BIT6;
    P3SEL |= BIT6;
    P3SEL2 &= ~(BIT6);

    TACCR0 = current_base_value; // 16MHz / 8 / 100 Hz
    // TACCR0 = current_base_value << 4; 
    TACCTL2 = OUTMOD_7; // select reset/set mode
    TACCR2 = current_base_value >> 1; // half of TACCR0
    // TACCR2 = current_base_value << 3; // half of TACCR0


    TACTL = MC0 + TASSEL1 + TACLR; // trigger timer start, SMCLK, mode 0 (count-up)
}


void pwm_dutycycle(unsigned char value){
    TACCR0 = current_base_value;
    TACCR2 = dutycycle_step * value; // will be rounded to int
    dutycycle = value;
}


void pwm_freq(unsigned char value){
    current_base_value = base_reg_value - value * step;
    TACCR0 = current_base_value;
    // TACCR2 = current_target_value >> 1; // divided by 2
    pwm_dutycycle(dutycycle);
}
void pwm_stop(){
    TACCR0 = 0;
}
