/*
 * server.h
 *
 *  Created on: 2019�~6��21��
 *      Author: hp5588
 */

#ifndef LIBS_SERVER_H_
#define LIBS_SERVER_H_
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "./LCD.h"
#include "./led.h"
#include "./flash.h"
#include "./uart.h"

#define FLASH_DATA_ADDR (FLASH_SEC1_BGN_ADDR)

#define SRV_TYPE_MMA_X  0X00
#define SRV_TYPE_MMA_Y  0X01
#define SRV_TYPE_MMA_Z  0X02

#define SRV_TYPE_POT    0X03
#define SRV_TYPE_LDR    0X04
#define SRV_TYPE_NTC    0X05

#define SRV_TYPE_JS_X   0X06
#define SRV_TYPE_JS_Y   0X07
#define SRV_TYPE_JS_B   0X08

#define SRV_TYPE_US     0X09

#define SRV_TYPE_BUTTONS    0X0A

#define SRV_SYNC_0 0xFF 
#define SRV_SYNC_1 0xFE 
#define SRV_SYNC_2 0xFD
 
// packet: SYNC*3 | CMD | len | data
#define CLN_CMD_LED             0x00
#define CLN_CMD_RELAY           0x01
#define CLN_CMD_LCD_PUT_TEXT    0x02
#define CLN_CMD_LCD_CLEAR       0x03
#define CLN_CMD_LCD_HOME        0x04




typedef struct 
{
    uint8_t type;
    uint8_t len;
    uint8_t* value;
} SrvData;

typedef enum {
        STATE_SYNC_0,
        STATE_SYNC_1,
        STATE_SYNC_2,
        STATE_CMD,
        STATE_LEN,
        STATE_DATA,
        STATE_END
} RxState;

void server_init();

void server_update();

void server_append(uint8_t typ, uint8_t *value);
void server_clear();


#endif /* LIBS_SERVER_H_ */
