/***************************************************************************//**
 * @file    i2c.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    24.05.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 2 - I2C
 *
 * Here goes a detailed description if required.
 ******************************************************************************/

#include "./i2c.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/

#define LED_R_ON     P1OUT |= BIT0;
#define LED_R_OFF    P1OUT &= ~BIT0;
#define LED_R_TG     P1OUT ^= BIT0;

// A variable to be set by your interrupt service routine:
// 1 if all bytes have been sent, 0 if transmission is still ongoing.
unsigned char transferFinished = 0;
unsigned char readyToRead = 0;


/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/
unsigned char lastOperation = STOP;

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/
void i2c_enter_start_condition(bool isTransmitter){

    if (isTransmitter){
        UCB0CTL1 |= UCTR; // transmitter mode
        lastOperation = WRITE;
    }
    else{
        UCB0CTL1 &= ~UCTR; // receiver mode
        lastOperation = READ;
    }
    UCB0CTL1 |= UCTXSTT; //  trigger START condition

    // in logic analyzer ACK is seen, but this bit is never cleared internally
    // skip checking here
//   while(UCB0CTL1 & UCTXSTT); // wait for ACK from slave after address is sent

}
void i2c_enter_stop_condition(){
    if (lastOperation == STOP)
        return;

    UCB0CTL1 |= UCTXSTP; // trigger STOP condition
    while(UCB0CTL1 & UCTXSTP); // wait until receive the ACK from slave
    lastOperation = STOP;
}


/******************************************************************************
 * FUNCTION IMPLEMENTATION
 *****************************************************************************/

// TODO: Implement these functions:

void i2c_init (unsigned char addr) {

    // connect pin to I2C bus
    P3DIR |= I2C_EN;
    P3OUT |= I2C_EN; 

    // select I2C peripheral for pins P1.6 and P1.7
    P1SEL |=  SCL + SDA;
    P1SEL2|=  SCL + SDA;

    UCB0CTL1 |= UCSWRST; // hold in soft-reset

    UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC; // 7-bit by default, Master Mode, I2C mode , Sync mode
    UCB0CTL1 = UCSSEL_2 + UCSWRST; // select SMCLK as clock source and hold the timer to reset status


    // pre-scaler for clock 16MHz => 100kHz ( divide by 160 [0xA0] )
    UCB0BR0 = 0x50; 
    UCB0BR1 = 0;

    UCB0I2CSA = addr & 0x7F; // write 7-bit slave address (mask out 7th bit)

    UCB0CTL1 &= ~UCSWRST; // disable soft reset


    // UCB0I2CIE = UCNACKIE; // enable NACK interrupt
    IE2 |= UCB0RXIE + UCB0TXIE; // enable RX interrupt, TX interrupt

}


static unsigned char NACK_FLAG = false;
unsigned char i2c_write(unsigned char length, unsigned char * txData, unsigned char stop) {
    // the STOP-condition is already checked in i2c_enter_stop_condition()

    transferFinished = 0;

    if (lastOperation != WRITE){
        if (lastOperation != STOP)
            i2c_enter_stop_condition();
        i2c_enter_start_condition(true);
    }

    uint8_t i = 0;
    for(; i< length;i++){
        transferFinished = 0;
        UCB0TXBUF = txData[i]; // write data to TX buffer
        while(!transferFinished); // wait until the UCB0TXIFG ( reday to send next byte)
        if (NACK_FLAG)
            return 1;
    }


    if (stop){
        i2c_enter_stop_condition();
    }

    return 0;
}

void i2c_read(unsigned char length, unsigned char * rxData) {

    // restart condition
    i2c_enter_start_condition(false);
    
    uint8_t i = 0;
    for(; i< length;i++){
        if (length-1 == i){
             P1OUT ^= BIT4;
            __delay_cycles(400); // delay 50 us
             P1OUT ^= BIT4;
            i2c_enter_stop_condition();
        }
        while(!readyToRead); // wait until the RX is ready to receive
        readyToRead = 0;
        rxData[i] = UCB0RXBUF; // read data from RX buffer
    }


    // Wait for transfer to be finished.
    i2c_enter_stop_condition();

}


// ISR
void I2C_USCIAB0TX_ISR()
{

    if (IFG2 & UCB0TXIFG){
        transferFinished = 1;
        IFG2 &= ~UCB0TXIFG;
    }

    if (IFG2 & UCB0RXIFG){
        readyToRead = 1;
        IFG2 &= ~UCB0RXIFG;
    }

    if (lastOperation == READ){
        LED_R_ON
    }

}


unsigned char receivedBuf;
void I2C_USCIAB0RX_ISR()
{


    if (UCB0STAT & UCNACKIFG){
        // reset the transmission
        i2c_enter_stop_condition();
        readyToRead = 0;
        transferFinished = 0;
        NACK_FLAG = 1;
        transferFinished = 1;
    }
}

void I2C_USCIAB0_ISR(uint8_t dir){
    if (dir){
        I2C_USCIAB0TX_ISR();
    }else{
        I2C_USCIAB0RX_ISR();
    }
}

