/*
 * led.c
 *
 *  Created on: 2019�~6��23��
 *      Author: hp5588
 */
#include "./led.h"

#define CLK_HIGH P2OUT |= BIT4; // clk LOW
#define CLK_LOW  P2OUT &= ~BIT4; // clk LOW

#define SERIAL_MODE  P2OUT |= BIT0; P2OUT &= ~BIT1;
#define PARALLEL_MODE   P2OUT |= BIT0 + BIT1; // parellel load mode
#define HOLD_MODE   P2OUT &= ~ (BIT0 + BIT1); // parellel load mode

#define CLR_HOLD P2OUT &= ~BIT5;
#define CLR_RELEASE P2OUT |= BIT5;

#define SR_HIGH P2OUT |= BIT6;
#define SR_LOW  P2OUT &= ~BIT6;


// shared with ultrasonic
#define GET_P2_4  P2DIR|=BIT4;  P2SEL &= ~BIT4;    P2SEL2 &= ~BIT4;

uint8_t _leds_status = 0;

void _led_latch_data(){
    CLK_LOW
    buttons_latch_delay();
    CLK_HIGH
}

void _led_clear_load(){
    CLK_LOW
    // CLR_HOLD
    P2OUT &= ~BIT5;
    buttons_clear_delay();
    P2OUT |= BIT5;
    // CLR_RELEASE
}

void led_init(){
    // S0(P2.0), S1(P2.1), CK(P2.4), CLR(P2.5), SR(P2.6)
    P2DIR |= BIT0 + BIT1 + BIT4 + BIT5 + BIT6; 

    P2SEL &= ~BIT6;
    P2SEL2 &= ~BIT6;

    // setup 74HC194 shift register
    SERIAL_MODE // parellel load mode
    _led_clear_load();
}
void led_set(uint8_t leds, bool on){
    GET_P2_4
    SERIAL_MODE
    
    uint8_t value = on ? 0XFF: 0x00;
    _leds_status = (_leds_status & ~leds) + (value & leds);

    uint8_t i = 0;
    for (;i<4;i++){
        if((_leds_status & (0x01 << (3-i))) ){
                SR_HIGH
            }else{
                SR_LOW
            }
        _led_latch_data();
    }
    
    HOLD_MODE
    // PARALLEL_MODE

    // _led_latch_data();

}
