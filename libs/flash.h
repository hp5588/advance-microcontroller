/***************************************************************************//**
 * @file    flash.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    16.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 4 - SPI
 *
 * Might need to erase sector before writing to flash.
 * (calling flash_erase_sector() prior to flash_write())
 ******************************************************************************/

#ifndef LIBS_FLASH_H_
#define LIBS_FLASH_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "./spi.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
/******************************************************************************
 * CONSTANTS
 *****************************************************************************/
#define  FLASH_SEC0_BGN_ADDR (0x00000000)
#define  FLASH_SEC1_BGN_ADDR (0x00010000)
#define  FLASH_SEC2_BGN_ADDR (0x00020000)
#define  FLASH_SEC3_BGN_ADDR (0x00030000)
#define  FLASH_SEC4_BGN_ADDR (0x00040000)
#define  FLASH_SEC5_BGN_ADDR (0x00050000)

#define  FLASH_SEC0_END_ADDR (0x0000FFFF)
#define  FLASH_SEC1_END_ADDR (0x0001FFFF)
#define  FLASH_SEC2_END_ADDR (0x0002FFFF)
#define  FLASH_SEC3_END_ADDR (0x0003FFFF)
#define  FLASH_SEC4_END_ADDR (0x0004FFFF)
#define  FLASH_SEC5_END_ADDR (0x0005FFFF)

#define CMD_WRITE_ENABLE                    (0x06)
#define CMD_WRITE_DISABLE                   (0x04)
#define CMD_READ_IDENTIFICATION             (0x9F)
#define CMD_READ_STATUS_REGISTER            (0x05)
#define CMD_WRITE_STATUS_REGISTER           (0x01)
#define CMD_READ_DATA_BYTES                 (0x03)
#define CMD_PAGE_PROGRAM                    (0x02)
#define CMD_SECTOR_ERASE                    (0xD8)
#define CMD_BULK_ERASE                      (0xC7)
#define CMD_DEEP_POWER_DOWN                 (0xB9)
#define CMD_RELEASE_FROM_DEEP_POWER_DOWN    (0xAB)

// registers 
#define WIP BIT0
#define WEL BIT1
/******************************************************************************
 * VARIABLES
 *****************************************************************************/



/******************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/
void flash_erase_sector(long int address);

// Initialise the flash chip (in case you need it, else leave this function
// empty).
void flash_init(void);

// Read <length> bytes into <rxData> starting from address <address> (1 pt.)
void flash_read(long int address, unsigned char length, unsigned char * rxData);

// Write <length> bytes from <txData>, starting at address <address> (1 pt.)
void flash_write(long int address, unsigned char length, unsigned char * txData);

// Returns 1 if the FLASH is busy or 0 if not.
// Note: this is optional. You will probably need this, but you don't have to
// implement this if you solve it differently.
unsigned char flash_busy(void);

#endif /* LIBS_FLASH_H_ */
