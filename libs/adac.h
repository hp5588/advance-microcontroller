/***************************************************************************//**
 * @file    adac.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    24.05.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 2 - I2C
 *
 * Here goes a detailed description if required.
 ******************************************************************************/

#ifndef EXERCISE_LIBS_ADAC_H_
#define EXERCISE_LIBS_ADAC_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "./i2c.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/******************************************************************************
 * CONSTANTS
 *****************************************************************************/
#define CHANNEL0 0x00
#define CHANNEL1 0x01
#define CHANNEL2 0x02
#define CHANNEL3 0x03

#define AUTO_INC 0x04

#define INPUT_MODE0 0x00
#define INPUT_MODE1 0x10
#define INPUT_MODE2 0x20
#define INPUT_MODE3 0x30

#define OUTPUT_EN 0x40



/******************************************************************************
 * VARIABLES
 *****************************************************************************/


/******************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/

// All functions return 0 if everything went fine
// and anything but 0 if not.

// Initialize the ADC / DAC
unsigned char adac_init(void);

// Read all ADC-values and write it into the passed values-array.
// (Important: always pass an array of size four (at least).) (1 pt.)
unsigned char adac_read(unsigned char * values);

// Write a certain value to the DAC. (1 pt.)
unsigned char adac_write(unsigned char value);

#endif /* EXERCISE_LIBS_ADAC_H_ */
