/*
 * US.h
 *
 *  Created on: 2019�~6��20��
 *      Author: hp5588
 */

#ifndef LIBS_US_H_
#define LIBS_US_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "./pwm.h"
// ultrasonic distance senosor

void us_init();


// Notice: blocking function
float us_get_distance(); // unit: cm



#endif /* LIBS_US_H_ */
