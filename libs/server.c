/*
 * @file    server.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    24.05.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 2 - I2C
 */
#include "./server.h"


SrvData _data[11];
uint8_t _pos = 0;

RxState _rx_state = STATE_SYNC_0;
// data of current attribute
uint8_t _rx_cmd = 0;
uint8_t _rx_len = 0;
uint8_t _rx_data[20];
void _server_rx_handler();

void _msg_wrtie_to_flash(char * data, uint8_t len);

void server_init(){

}


void server_update(){

    //send sync bytes
    serialWrite(SRV_SYNC_0);
    serialWrite(SRV_SYNC_1);
    serialWrite(SRV_SYNC_2);


    uint8_t i = 0;
    for (; i < _pos; i++)
    {
        uint8_t type = _data[i].type;
        uint8_t *dp = _data[i].value;
        uint8_t len = _data[i].len;

        serialWrite(type);
        serialWrite(len);
        
        uint8_t i=0;
        for (; i < len; i++)
        {
            serialWrite(*(dp + (len-1) - i));
        }
    }


    // process all data in UART rx buffer
    _server_rx_handler();
}

void server_append(uint8_t typ, uint8_t *value){
    if (_pos < sizeof(_data)){
        _data[_pos].type = typ;
        _data[_pos].value = value;
        switch (typ)
        {
            case SRV_TYPE_MMA_X:  
            case SRV_TYPE_MMA_Y:  
            case SRV_TYPE_MMA_Z:
            case SRV_TYPE_POT:
            case SRV_TYPE_LDR:
            case SRV_TYPE_NTC:
                // 16 bit
                _data[_pos].len = 2;
                break;
        
            case SRV_TYPE_JS_X:   
            case SRV_TYPE_JS_Y:   
            case SRV_TYPE_JS_B:   
            case SRV_TYPE_US:     
            case SRV_TYPE_BUTTONS: 
                // 8 bit
                _data[_pos].len = 1;
                break;
            
            default:
                break;
        }
        _pos++;
    }
}

void server_clear(){
    _pos = 0;
}

void _server_rx_handler(){
    static uint8_t rx_count = 0; 
    while (true)
    {
        int byte = serialRead();
        if (byte < 0)
            return;
        
        switch (_rx_state)
        {
            case STATE_SYNC_0:
                rx_count = 0;
                _rx_state = (byte == SRV_SYNC_0) ? STATE_SYNC_1 : STATE_SYNC_0;
                break;
            case STATE_SYNC_1:
                _rx_state = (byte == SRV_SYNC_1) ? STATE_SYNC_2 : STATE_SYNC_0;
                break;
            case STATE_SYNC_2:
                _rx_state = (byte == SRV_SYNC_2) ? STATE_CMD : STATE_SYNC_0;
                break;
            case STATE_CMD:
                _rx_cmd = byte;
                _rx_state = STATE_LEN;
                break;

            case STATE_LEN:
                _rx_len = byte;
                if (_rx_len > 0)
                    _rx_state = STATE_DATA;
                else
                    _rx_state = STATE_END;

                break;
            
            case STATE_DATA:
                _rx_data[rx_count++] = byte;
                if (rx_count >= _rx_len)
                    _rx_state = STATE_END;

                break;
            
            
            default:
                break;
        }

        if (_rx_state == STATE_END){
            // complete data is collected and process accordingly
            switch (_rx_cmd)
            {
            case CLN_CMD_LCD_PUT_TEXT:
                _rx_data[_rx_len] = 0x00;
                lcd_clear();
                lcd_putText((char *)_rx_data);
                _msg_wrtie_to_flash(_rx_data, _rx_len);

                
                
                break;
            case CLN_CMD_LCD_CLEAR:
                lcd_clear();
                lcd_cursorSet(0,0);

                break;
            case CLN_CMD_LED:
                led_set(_rx_data[1], _rx_data[0] > 0);
            
            default:
                break;
            }

            _rx_state = STATE_SYNC_0;
        }

    }


}
void _msg_wrtie_to_flash(char * data, uint8_t len){
    spi_init();
    flash_erase_sector(FLASH_DATA_ADDR);
    flash_write(FLASH_DATA_ADDR, len, data);
    
}
