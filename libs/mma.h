/***************************************************************************//**
 * @file    mma.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    06.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 3 - Advanced Sensors
 * 
 *
 * Here goes a detailed description if required.

 ******************************************************************************/

#ifndef EXERCISE_3_LIBS_MMA_H_
#define EXERCISE_3_LIBS_MMA_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include "./i2c.h"
#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/******************************************************************************
 * CONSTANTS
 *****************************************************************************/


// registers
#define MMA_REG_OUT_X_MSB 0x01
#define MMA_REG_OUT_X_LSB 0x02
#define MMA_REG_OUT_Y_MSB 0x03
#define MMA_REG_OUT_Y_LSB 0x04
#define MMA_REG_OUT_Z_MSB 0x05
#define MMA_REG_OUT_Z_LSB 0x06

#define MMA_REG_F_SETUP 0x09
#define MMA_REG_SYSMOD 0x0B

#define MMA_REG_INT_SRC 0x0C
#define MMA_REG_XYZ_DATA_CFG 0x0E

#define MMA_REG_PULSE_CFG 0x21
#define MMA_REG_PULSE_SRC 0x22
#define MMA_REG_PULSE_THSZ 0x25
#define MMA_REG_PULSE_TMLT 0x26
#define MMA_REG_PULSE_LTCY 0x27
#define MMA_REG_PULSE_WIND 0x28
#define MMA_REG_CTRL_REG1 0x2A
#define MMA_REG_CTRL_REG2 0x2B
#define MMA_REG_CTRL_REG3 0x2C
#define MMA_REG_CTRL_REG4 0x2D
#define MMA_REG_CTRL_REG5 0x2E



//MMA_INT_SRC
#define SRC_PULSE BIT3

//MMA_F_SETUP
#define F_MODE0 BIT6 
#define F_MODE1 BIT7

//MMA_XYZ_DATA_CFG
// 0 0 2g
// 0 1 4g
// 1 0 8g
// 1 1 Reserved
#define FS1 BIT1
#define FS0 BIT0

//MMA_REG_PULSE_CFG
#define DPA BIT7
#define ELE BIT6
#define ZDPEFE BIT5

// MMA_REG_PULSE_SRC
#define AxZ BIT6

// MMA_REG_PULSE_THSZ
// 0x40 -> 64 (50%)

// MMA_REG_PULSE_TMLT
// MMA_REG_PULSE_LTCY
// MMA_REG_PULSE_WIND

//MMA_REG_CTRL_REG1
#define F_READ BIT1
#define ACTIVE BIT0


//MMA_REG_CTRL_REG2
#define ST BIT7 // self-test
#define RST BIT6 // self-test

//MMA_REG_CTRL_REG3
#define IPOL BIT1

//MMA_REG_CTRL_REG4
#define INT_EN_ASLP BIT7
#define INT_EN_PULSE BIT3

//MMA_REG_CTRL_REG5
#define INT_CFG_PULSE BIT3 // 0: routed to INT2 / 1: routed to INT1


/******************************************************************************
 * VARIABLES
 *****************************************************************************/
typedef struct
{
    /* data */
    uint8_t X_MSB;
    uint8_t X_LSB;
    uint8_t Y_MSB;
    uint8_t Y_LSB;
    uint8_t Z_MSB;
    uint8_t Z_LSB;
} SData; // long data

typedef struct
{
    /* data */
    uint16_t F_X;
    uint16_t F_Y;
    uint16_t F_Z;
} FData; // full data

typedef union
{
    FData fdata; // dast data
    SData sdata; // full data
} Data;



/******************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/
void mma_take_snapshot();
uint8_t mma_snapshot_diff();
// All configuration functions return 0 if everything went fine
// and anything but 0 if not (they are the ones with a unsigned char return type).

// Initialize the MMA with 8 bit resolution and 4G measurement range (1 pt.)
unsigned char mma_init(void);

// Change the measurement range. (0: 2g, 1: 4g, >1: 8g) (0.5 pt.)
unsigned char mma_setRange(unsigned char range);
// Change the resolution (0: 8 Bit, >= 1: 14 Bit) (0.5 pt.)
unsigned char mma_setResolution(unsigned char resolution);

// Run a self-test on the MMA, verifying that all three axis and all three
// measurement ranges are working. (1 pt.)
/* HINT:
 * The idea of the self test is that you measure the current acceleration values,
 * then enable the on-chip self-test and then read the values again.
 * The values without selftest enabled and those with selftest enabled
 * should now feature a predefined difference (see the datasheet).
 */
unsigned char mma_selftest(void);


// Set up the double tap interrupt on the MMA (do not set up the interrupt on
// the MSP in this function!). This means that the MMA should change the INT1-
// pin whenever a double tap is detected. You may freely choose the axis on
// which the tap has to be received. (You should put a comment in your code,
// which axis you chose, though). (1 pt.)
/* HINT:
 * As the datasheet for the MMA is a bit stingy when it comes to the double
 * tap stuff, so here's (roughly) what you should do:
 *
 *  1) Go to standby (as you can only change the registers when in standby)
 *  2) Write MMA_PULSE_CFG  to enable the z-axis for double tap
 *  3) Write MMA_PULSE_THSZ to set the tap threshold (e.g. to 2g)
 *  4) Write MMA_PULSE_TMLT to set the pulse time limit (e.g. to 100 ms)
 *  5) Write MMA_PULSE_LTCY to set the pulse latency timer (e.g. to 200 ms)
 *  6) Write MMA_PULSE_WIND to set the time window for the second tap
 *  7) Write MMA_CTRL_REG4  to set the pulse interrupt
 *  8) Write MMA_CTRL_REG5  to activate the interrupt on INT1
 *  9) Write MMA_CTRL_REG3  to set the interrupt polarity
 * 10) Return to active mode
 */
unsigned char mma_enableTapInterrupt(void);
// Disable the double-tap-interrupt on the MMA. (0.5 pt.)
unsigned char mma_disableTapInterrupt(void);


// Read the values of all three axis from the chip and store the values
// internally. Take the requested resolution into account. (1 pt.)
unsigned char mma_read(void);


/* Get Functions (1 pt. total): */

// Return the appropriate 8 bit values
// If the resolution during mma_read was 14 bit, translate the data to 8 bit
signed char mma_get8X(void);
signed char mma_get8Y(void);
signed char mma_get8Z(void);

// Return the appropriate 14 bit values
// If the resolution during mma_read was 8 bit, translate the data to 14 bit
int mma_get14X(void);
int mma_get14Y(void);
int mma_get14Z(void);

// Return the appropriate values in m*s^-2.
double mma_getRealX(void);
double mma_getRealY(void);
double mma_getRealZ(void);

#endif /* EXERCISE_3_LIBS_MMA_H_ */
