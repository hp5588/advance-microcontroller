/*
 * buttons.h
 *
 *  Created on: 2019�~6��20��
 *      Author: hp5588
 */

#ifndef LIBS_BUTTONS_H_
#define LIBS_BUTTONS_H_
#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "./helper.h"

// TODO: remove later
#include "./uart.h"
#define BUTTON_PB1 BIT0
#define BUTTON_PB2 BIT1
#define BUTTON_PB3 BIT2
#define BUTTON_PB4 BIT3
#define BUTTON_PB5 BIT4
#define BUTTON_PB6 BIT5

void buttons_init();

//  x -> undefined
// @return byte
//  x x PB6 PB5 PB4 PB3 PB2 PB1 
uint8_t buttons_read();




#endif /* LIBS_BUTTONS_H_ */
