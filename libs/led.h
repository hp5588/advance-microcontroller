/*
 * led.h
 *
 *  Created on: 2019�~6��23��
 *      Author: hp5588
 */

#ifndef LIBS_LED_H_
#define LIBS_LED_H_
#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "./helper.h"


#define LED_D1 BIT0
#define LED_D2 BIT1
#define LED_D3 BIT2
#define LED_D4 BIT3

void led_init();
void led_set(uint8_t leds, bool on);

#endif /* LIBS_LED_H_ */
