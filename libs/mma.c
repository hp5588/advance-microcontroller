/***************************************************************************/ /**
 * @file    mma.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    06.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 3 - Advanced Sensors
 * Library for MMA8451
 * Incldue snapshot feature to save current read value for comparison later
 * @mma_snapshot_diff() gives the evaluated x,y,z value of how different the data is 
 * from previous snapshot.
 *  
 *
 * Here goes a detailed description if required.
 ******************************************************************************/

#include "./mma.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/
 Data xyzData = {};
 Data dataSnapshot = {};


 // 2g: 0.25 mg / 4g: 0.5mg / 8g: 1.0mg
 double ratioTable[] = {0.00025, 0.0005, 0.001};

 static uint8_t mmaRange = 0;
 static bool mmaIsFastMode = false;

/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/
// single byte read / write
void mma_read_reg_values(unsigned char reg, unsigned char length, unsigned char *data);
uint8_t mma_read_reg_value(unsigned char reg);

// write 2 byte max
void mma_write_reg_value(unsigned char reg, unsigned char data);

// mma operation function
void mma_set_active(bool active);

/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/

void mma_read_reg_values(unsigned char reg, unsigned char length, unsigned char *data)
{
    i2c_write(1, &reg, false);
    i2c_read(length, data);
}
uint8_t mma_read_reg_value(unsigned char reg){
    uint8_t data;
    mma_read_reg_values(reg, 1, &data);
    return data;
}
 
void mma_write_reg_value(unsigned char reg, unsigned char data){
    uint8_t regData[] = {reg, data};
    i2c_write(sizeof(regData), regData, true);
}

void mma_set_active(bool active){
    // active = 0 > standby
    // active = 1 > active
    mma_write_reg_value(MMA_REG_CTRL_REG1, 
        active | (mmaIsFastMode << 1)
    );
}

/******************************************************************************
 * FUNCTION IMPLEMENTATION
 *****************************************************************************/

void mma_take_snapshot(){
    dataSnapshot = xyzData;
}
// calculate the difference between snapshot value and live value
// then map the differnce range from 1 to 100 level 
uint8_t mma_snapshot_diff(){
    uint16_t diff = 0;
    diff += abs(xyzData.sdata.X_MSB - dataSnapshot.sdata.X_MSB);
    diff += abs(xyzData.sdata.Y_MSB - dataSnapshot.sdata.Y_MSB);
    diff += abs(xyzData.sdata.Z_MSB - dataSnapshot.sdata.Z_MSB);
    return (uint8_t)(int)(diff / 7.68); // 256 * 3 / 100 (percentage of max value)
}




unsigned char mma_init(void)
{
    // i2c_init(MMA_ADDR);

    //put in stanby mode 
    mma_set_active(false);

    //enable double tap
    mma_enableTapInterrupt();

    // set resolution to 8-bit 
    mma_setResolution(1);
    
    // set range to 4g
    mma_setRange(1);

    mma_set_active(true);

    return 0;
}


// 0 0 ->2
// 0 1 ->4
// 1 0 ->8
// 1 1 ->reserved
unsigned char mma_setRange(unsigned char range)
{   
    if (range > 2)
        range = 2;
    
    mmaRange = range;

    unsigned char data[] = {MMA_REG_XYZ_DATA_CFG, range};
    i2c_write(sizeof(data), data, true);
    return 0;
}
unsigned char mma_setResolution(unsigned char resolution)
{
    
    if (resolution > 0)
    {
        // 14-bit mode
        mmaIsFastMode = false;
    }
    else {
        // 8-bit mode
        mmaIsFastMode = true;
    }    
    return 0;
}

unsigned char mma_selftest(void)
{   
    mma_set_active(true);

    mma_read();
//    Data xyzDataSnapshot = xyzData;
    mma_write_reg_value(MMA_REG_CTRL_REG2, ST);
    mma_read();
    
    // my MMA has similar reading value no matter self-test is on or not :/
    // compare value xyzData and xyzDataSnapshot
    // persudo code
    // if ((xyzData.sdata.X_MSB - xyzDataSnapshot.sdata.X_MSB) < 181)
    //      return -1;
    // if ((xyzData.sdata.Y_MSB - xyzDataSnapshot.sdata.Y_MSB) < 255)
    //      return -1;
    // if ((xyzData.sdata.Z_MSB - xyzDataSnapshot.sdata.Z_MSB) < 1680)
    //      return -1;
    
    // turn off self test
    mma_write_reg_value(MMA_REG_CTRL_REG2, 0x00);
    return 0;
}
unsigned char mma_enableTapInterrupt(void)
{
    // choose z-axis to detect double tap
    mma_write_reg_value(MMA_REG_PULSE_CFG, ZDPEFE);
    
    // 1g / 0.063g = 16 (0x10)
    mma_write_reg_value(MMA_REG_PULSE_THSZ, 0x08);

    // LPF disabled @ ORD=800Hz -> step = 0.625ms
    // 100ms / 0.625ms = 160 (0xA0)
    mma_write_reg_value(MMA_REG_PULSE_TMLT, 0xA0);

    // LPF disabled @ ORD=800Hz -> step = 1.25ms
    // 200ms / 1.25ms = 160 (0xA0)
    mma_write_reg_value(MMA_REG_PULSE_LTCY, 0xA0);

    // LPF disabled @ ORD=800Hz -> step = 1.25ms
    // 300ms / 1.25ms = 240 (0xF0)
    mma_write_reg_value(MMA_REG_PULSE_WIND, 0xF0);

    // enable pulse detection interrupt
    mma_write_reg_value(MMA_REG_CTRL_REG4, INT_EN_PULSE);

    // set INT1 pin for pulse interupt
    mma_write_reg_value(MMA_REG_CTRL_REG5, INT_CFG_PULSE);

    //set to active high
    mma_write_reg_value(MMA_REG_CTRL_REG3, IPOL);

    return 0;
}
unsigned char mma_disableTapInterrupt(void)
{
    // disable z double tap interrupts
    mma_write_reg_value(MMA_REG_CTRL_REG4, 
        mma_read_reg_value(MMA_REG_CTRL_REG4) & (~INT_EN_PULSE)
    );
    return 0;
}

unsigned char mma_read(void)
{
    // clean buffer
    memset(xyzData, 0x00, sizeof(xyzData));

    // write reg addr
    uint8_t data[] = {MMA_REG_OUT_X_MSB};
    i2c_write(sizeof(data), data, false);

    // read data
    if (mmaIsFastMode){
        uint8_t buffer[3];
        i2c_read(sizeof(buffer), buffer);
        xyzData.sdata.X_MSB =buffer[0];
        xyzData.sdata.Y_MSB =buffer[1];
        xyzData.sdata.Z_MSB =buffer[2];
    }else
    {
        i2c_read(6, (uint8_t *)&xyzData);
    }
    return 0;
}

signed char mma_get8X(void)
{
    return (signed char)xyzData.sdata.X_MSB;
}
signed char mma_get8Y(void)
{
    return (signed char)xyzData.sdata.Y_MSB;
}
signed char mma_get8Z(void)
{
    return (signed char)xyzData.sdata.Z_MSB;
}

int mma_get14X(void)
{
    return (int16_t)(xyzData.sdata.X_MSB << 8 | xyzData.sdata.X_LSB)>>2;
}
int mma_get14Y(void)
{
    return (int16_t)(xyzData.sdata.Y_MSB << 8 | xyzData.sdata.Y_LSB)>>2;
}
int mma_get14Z(void)
{
    return (int16_t)(xyzData.sdata.Z_MSB << 8 | xyzData.sdata.Z_LSB)>>2;
}

double mma_getRealX(void)
{
    return mma_get14X() * ratioTable[mmaRange];
}
double mma_getRealY(void)
{
    return mma_get14Y() * ratioTable[mmaRange];
}

double mma_getRealZ(void)
{
    return mma_get14Z() * ratioTable[mmaRange];
}
