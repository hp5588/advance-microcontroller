/*
 * pwm.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    24.05.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 2 - I2C
 *
 * generate PWM signal from 100 Hz to 800 Hz with 50% duty cycle
 * can be disabled by calling pwm_stop()
 */

#ifndef LIBS_PWM_H_
#define LIBS_PWM_H_
#include <msp430g2553.h>
#include <stdint.h>

void pwm_init();

// min 100 Hz
// max 800 Hz
// 256 steps (value from 0 - 255)
void pwm_freq(unsigned char value);

// @value: 0-100 percent
void pwm_dutycycle(unsigned char value);


// stop the PWM by writing 0 into TACCR0
void pwm_stop();



#endif /* LIBS_PWM_H_ */
