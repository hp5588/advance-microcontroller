/*
 * adc.c
 *
 *  Created on: 2019�~6��20��
 *      Author: hp5588
 */


#include "./adc.h"

void adc_init(){
    ADC10CTL0 = ADC10SHT_2 + ADC10ON; // ADC10ON, interrupt enabled
    ADC10AE0 |= BIT0 + BIT3 + BIT4;
}

uint16_t adc_read_channel(uint8_t channel){
    uint16_t reg = channel;

    // disable ADC before reconfig
    ADC10CTL0 &= ~ENC;
    ADC10CTL1 = (reg << 12);    //set input chennel
    ADC10CTL0 |= ENC + ADC10SC + ADC10ON; // sample and start conversion
//    serialPrintInt(reg);

    
    while (ADC10CTL1 & ADC10BUSY); // wait until conversion complete

    return ADC10MEM;
}
