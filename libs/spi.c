/*
 * @file    spi.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    16.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 4 - SPI
 */
#include "./spi.h"

void spi_init(void){
    // init pins
    // switch SPI bus connection 
    P3OUT &= ~BIT5; 
    P3DIR |= BIT5;

    // P3.4 CS
    P3OUT |= BIT4;
    P3DIR |= BIT4;

    // P1.5 CLK, P1.6 MI, P1.7 MO
    P1SEL |= BIT5 + BIT6 + BIT7;
    P1SEL2 |= BIT5 + BIT6 + BIT7;

    // init SPI module
    UCB0CTL1 |= UCSWRST; // hold in soft-reset
    
    // capture on first edge (inactive low) 
    // MSB First (8-bit), Master, (3-pin mode), Synchronous
    UCB0CTL0 = UCCKPH + UCMSB + UCMST + UCSYNC;
    UCB0CTL1 = UCSSEL_2;                     // SMCLK
    UCB0BR0 = 0;    UCB0BR1 = 0;              // @ 1MHz                             
                                 
    UCB0CTL1 &= ~UCSWRST;                     // start SPI
//    IE2 |= UCB0RXIE;                          // enable RX interrupts

}

void spi_read(unsigned char length, unsigned char * rxData){

    uint8_t i = 0;
    for (; i < length; i++)
    {   
        while (!(IFG2 & UCB0TXIFG));
        UCB0TXBUF = 0x00; //send dummy byte to trigger transmission
        
        while (!(IFG2 & UCB0RXIFG));
        rxData[i] = UCB0RXBUF;
    }

}

unsigned int spi_read_byte(){
    uint8_t data;
    spi_read(1, &data);
    return data;
}



void spi_write(unsigned char length, unsigned char * txData){

    uint8_t i = 0;
    for (; i < length; i++)
    {
        while (!(IFG2 & UCB0TXIFG));
        UCB0TXBUF = txData[i];
    }
    
}


unsigned char spi_busy(void){
    // not used here
}

void SPI_USCIAB0_ISR(uint8_t dir){
    if (dir){
        SPI_USCIAB0TX_ISR();
    }else{
        SPI_USCIAB0RX_ISR();
    }
}

void SPI_USCIAB0TX_ISR(void)
{
    // not used here
}


void SPI_USCIAB0RX_ISR(void)
{
    // not used here
}