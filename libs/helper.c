/*
 * helper.c
 *
 *  Created on: 2019�~6��20��
 *      Author: hp5588
 */

#include "./helper.h"

void E_delay(){
    __delay_cycles(1 << CPU_CLOCK); // 1u second
}

void return_home_delay(){
    __delay_cycles(1500);
}

void buttons_latch_delay(){
    __delay_cycles(1 << CPU_CLOCK); //1 u
}

void buttons_clear_delay(){
    __delay_cycles(1 << CPU_CLOCK); //1 u
}

void std_delay_1ms(){
    __delay_cycles(1000 << CPU_CLOCK); //1 u
}
void std_delay(unsigned int milliseconds){
    unsigned int count = 0;
    for(;count < milliseconds; count++){
        std_delay_1ms();
    }
}
