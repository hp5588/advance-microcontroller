/***************************************************************************//**
 * @file    LCD.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    <date of creation>
 * @email   kaop@tf.uni-freiburg.de
 * @title   Exercise 1 - Display Interface
 * @brief   Low-level operation is implemented as local function for easier access to LCD
 *
 * Here goes a detailed description if required.
 ******************************************************************************/

#include "./LCD.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/
bool cursorOn = true;
bool cursorBlink = true;
bool displayOn = true;

static uint8_t cursor_x = 0;
static uint8_t cursor_y = 0;
/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/
/** bit operation function */
uint8_t set_bits(uint8_t in, uint8_t bits,uint8_t mask);

/** pin operation function */
/*1: high, 0: low*/
void set_RS(bool high);
void set_RW(bool high);
void set_E(bool high);
void set_E_ready();
void reset_E();


/* set data pins directions and output*/
void setup_data_pins_dir(bool output);
void setup_ctl_pin(RegSel regSelect);

/** data transfer function */
void write_byte(uint8_t byte, bool half);
uint8_t read_byte();

/** check state function */
bool is_busy();


/** instruction function */
void clear_display();
void return_home();
void set_entry_mode(EntryMode mode, bool displayShift);
void set_display_ctl(bool dspOn, bool cursorOn, bool blink);
void csr_dsp_shift(bool sc, bool rl);
void set_function(); /* only the first call works*/
void set_cgram_addr(uint8_t addr);
void set_ddram_addr(uint8_t addr);

void write_instruction(uint8_t instructionByte);

 /**  write and read from CGRAM/DDRAM */
void write_ram(uint8_t *data, uint8_t len);
void read_ram(uint8_t *data, uint8_t len);



/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/
/*
 *
 * Low-level pins operation
 *
 * */
/*set only bits in mask*/
uint8_t set_bits(uint8_t in, uint8_t set,uint8_t mask){
    uint8_t untouchBits = in & (~mask);
    uint8_t setBits = set & mask;
    return untouchBits | setBits;
}

void set_E(bool high){
    uint8_t out = high ? E:0;
    CTL_OUT = set_bits(CTL_OUT, out ,E_MASK);
}
void set_E_ready(){
    set_E(true);
    E_delay(); // wait for at least 500 ns
}
void reset_E(){
    set_E(false);
    E_delay(); // wait for at least 500 ns
}


void setup_data_pins_dir(bool output){
    uint8_t bits = output ? DATA_MASK: 0;
    DATA_DIR = set_bits(DATA_DIR, bits, DATA_MASK);
}

void setup_ctl_pin(RegSel regSelect){
    CTL_OUT = set_bits(CTL_OUT, regSelect ,CTL_MASK);
}


/* only last 4 bits are considered if half is enable*/
void write_byte(uint8_t byte, bool half){
    while(is_busy());

    // make sure E is LOW
    reset_E();
    setup_data_pins_dir(true);

    uint8_t data_H = (byte>>4) & DATA_MASK;
    uint8_t data_L = byte & DATA_MASK;

    uint8_t datas[2] = {data_H, data_L};

    uint8_t x = 0;
    uint8_t loop = half? 1:2;
    for (; x < loop; x++){

        //prepare D7-D4
        DATA_OUT = set_bits(DATA_OUT, datas[x], DATA_MASK);

        set_E_ready();
        reset_E();

    }

}

/*
 *
 * Application Functions
 *
 * */
bool is_busy(){
    // set control pins for busy check
    uint8_t ctlOutTemp = CTL_OUT;
//    CTL_OUT = set_bits(CTL_OUT, RW ,CTL_MASK);
    setup_ctl_pin(BF_READ);

    setup_data_pins_dir(false);


    set_E_ready();

    // read busy flag
    bool result = DATA_IN & BUSY_FLAG;

    // TODO: read address if needed

    // reset E after read finish
    reset_E();


    //read second part of data
    set_E_ready();
    reset_E();

    //restore control pins
    CTL_OUT = ctlOutTemp;


    return result;
}


/* Function set
 *       0   0   0   0   1   DL  N   F   *   *
 *
 *       default set as 4 bit mode (DL=0), 2 line (N=1, F=*)
 * */
void set_function(){

    setup_ctl_pin(IR_WRITE);
    uint8_t data = INS_FUNC_DATA_TMP | INS_FUNC_N;

    write_byte(data, true); // first instruction, 4 bit is enough to set 4-bit mode
    // wait 4.1 ms
//    std_delay(5);
    write_byte(data, false); // repeat full instruction

}

/* Display set
 *      0   0   0   0   1   D   C   B
 *
 * */
void set_display_ctl(bool dspOn, bool cursorOn, bool blink){
    uint8_t data = INS_DSP_CTL_DATA_TMP;
    data |= dspOn ? INS_DSP_CTL_D: 0;
    data |= cursorOn ? INS_DSP_CTL_C: 0;
    data |= blink ? INS_DSP_CTL_B: 0;

    write_instruction(data);
}
void return_home(){
    uint8_t data = INS_RTN_DATA_TMP;

    write_instruction(data);

    // wait for 1.5ms
    return_home_delay();
}

void set_entry_mode(EntryMode mode, bool displayShift){
    uint8_t data = INS_ENTRY_DATA_TMP;
    data |= (mode==INC) ? INS_ENTRY_ID: 0;
    data |= displayShift? INS_ENTRY_S: 0;

    write_instruction(data);
}

void csr_dsp_shift(bool sc, bool rl){
    uint8_t data = INS_SHIFT_DATA_TMP;
    data |= (sc) ? INS_SHIFT_SC: 0;
    data |= (rl) ? INS_SHIFT_RL: 0;

    write_instruction(data);
}
void clear_display(){
    uint8_t data = INS_CLR_DATA_TMP;

    write_instruction(data);
}
void set_cgram_addr(uint8_t addr){
    uint8_t data = INS_SET_CGRAM_DATA_TMP;
    data = set_bits(data, addr, INS_SET_CGRAM_MASK);

    write_instruction(data);
}

void set_ddram_addr(uint8_t addr){
    uint8_t data = INS_SET_DDRAM_DATA_TMP;
    data = set_bits(data, addr, INS_SET_DDRAM_MASK);

    write_instruction(data);
}


void write_instruction(uint8_t instructionByte){
    setup_ctl_pin(IR_WRITE);
    write_byte(instructionByte, false);
}

void write_ram(uint8_t *data, uint8_t len){
    setup_ctl_pin(DR_WRITE);

    uint8_t i;
    for (i=0; i < len; i++){
        write_byte(data[i], false);
    }
}
void read_ram(uint8_t *data, uint8_t len){
    // don't need here :)
}


uint8_t lcd_get_cursor_x(){
    return cursor_x;
}
uint8_t lcd_get_cursor_y(){
    return cursor_y;
}

/******************************************************************************
 * FUNCTION IMPLEMENTATION
 *****************************************************************************/

// TODO: Implement the functions.

void lcd_init (void){
    //init GPIO

    // set output register to LOW
    DATA_OUT = set_bits(DATA_OUT,0, DATA_MASK);
    CTL_OUT = set_bits(CTL_OUT,0, CTL_MASK|E_MASK);

    //set data port default to output (input when read)
    DATA_DIR = set_bits(DATA_DIR, DATA_MASK, DATA_MASK);
    //set control port to output
    CTL_DIR = set_bits(CTL_DIR, CTL_MASK|E_MASK, CTL_MASK|E_MASK);

//    std_delay(40);
    set_function();
    set_entry_mode(INC, false);
    set_display_ctl(displayOn, cursorOn, cursorBlink);
    return_home();
}


void lcd_enable (unsigned char on){
    displayOn = on;
    set_display_ctl(displayOn, cursorOn, cursorBlink);
}

void lcd_cursorSet (unsigned char x, unsigned char y){
    uint8_t addr = (y << 6) + x; // shift 6-bit equal to multiply 0x40
    set_ddram_addr(addr);

    cursor_x = x;
    cursor_y = y;
}

void lcd_cursorShow (unsigned char on){
    cursorOn = on;
    set_display_ctl(displayOn, cursorOn, cursorBlink);
}

void lcd_cursorBlink (unsigned char on){
    cursorBlink = on;
    set_display_ctl(displayOn, cursorOn, cursorBlink);
}

void lcd_clear (void){
    clear_display();
}

void lcd_putChar (char character){
    write_ram((uint8_t *)(&character),1);
}
void lcd_putText (char * text){
    uint8_t *p = (uint8_t *)text;

    // null char is the end of string
    while(*p != '\0'){
        lcd_putChar((char)*p);
        p++;
    }
}

void lcd_putNumber (int number){
    char str[MAX_INT_LEN];
    uint8_t len = sprintf(str, "%d", number);

    write_ram((uint8_t *)str, len);
}

void add_charactor(uint8_t addr, uint8_t* pattern, uint8_t len){
    // set the dest CGRAM address
    set_cgram_addr(addr);
    // write date to the target addr in CGRAM
    write_ram(pattern, len);

}

