/*
 * @file    joystock.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    16.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 4 - SPI
 */

#ifndef LIBS_JOYSTICK_H_
#define LIBS_JOYSTICK_H_

#include <stdio.h>
#include <stdlib.h>
#include "./adac.h"


typedef enum{
    JS_NONE,
    JS_UP,
    JS_DOWN,
    JS_LEFT,
    JS_RIGHT,
    JS_PRESS
} JsAction; // joystick action

// calibrate the neutral values
// give the no action status reading from ADC (x, y)
void js_calibrate(unsigned int xValue, unsigned int yValue);

// interprete the values as actions
// JS_PRESS has prority over other actions
JsAction js_convert_action(unsigned int xValue, unsigned int yValue, unsigned int bValue);

// set a neutral range that is interpred as JS_NONE (due to the floating ADC reading value)
void js_set_neutral_range(unsigned int range);

#endif /* LIBS_JOYSTICK_H_ */
