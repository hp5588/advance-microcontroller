/***************************************************************************//**
 * @file    adac.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    24.05.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 2 - I2C
 *
 * Here goes a detailed description if required.
 ******************************************************************************/

#include "./adac.h"

/******************************************************************************
 * VARIABLES
 *****************************************************************************/
#define CTL_BYTE  (CHANNEL0 | INPUT_MODE0 | OUTPUT_EN | AUTO_INC)




/******************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *****************************************************************************/



/******************************************************************************
 * LOCAL FUNCTION IMPLEMENTATION
 *****************************************************************************/



/******************************************************************************
 * FUNCTION IMPLEMENTATION
 *****************************************************************************/


unsigned char adac_init(){

    uint8_t ctlByte = CTL_BYTE;
    i2c_write(sizeof(ctlByte), &ctlByte, false);

    uint8_t porByte[4];
    i2c_read(4, porByte); // try to read the first time, 0x80 will be sent as first byte

}


unsigned char adac_read(unsigned char * values){
    i2c_read(4, values);
}

unsigned char adac_write(unsigned char value){
    uint8_t data[] = {CTL_BYTE, value};
    i2c_write(sizeof(data),data, false);
}

