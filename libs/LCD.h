/***************************************************************************//**
 * @file    LCD.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    <date of creation>
 * @email   kaop@tf.uni-freiburg.de
 * @title   Exercise 1 - Display Interface
 * @brief   The header define pins number for control use and data.
 *
 *
 * Here goes a detailed description if required.
 ******************************************************************************/

#ifndef LIBS_LCD_H_
#define LIBS_LCD_H_

/******************************************************************************
 * INCLUDES
 *****************************************************************************/

#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "helper.h"

/******************************************************************************
 * CONSTANTS
 *****************************************************************************/
/* chars */
#define CHAR_RETURN x17

/* data pins define*/
#define DATA_DIR P2DIR
#define DATA_OUT P2OUT
#define DATA_IN  P2IN

#define D4 BIT0
#define D5 BIT1
#define D6 BIT2
#define D7 BIT3

/*control pins define*/
#define CTL_DIR P3DIR
#define CTL_OUT P3OUT
#define RS      BIT0
#define RW      BIT1
#define E       BIT2

/*instruction control bit define*/
#define INS_ENTRY_ID     BIT1
#define INS_ENTRY_S     BIT0

#define INS_DSP_CTL_D   BIT2
#define INS_DSP_CTL_C   BIT1
#define INS_DSP_CTL_B   BIT0

#define INS_SHIFT_SC      BIT3
#define INS_SHIFT_RL      BIT2

#define INS_FUNC_DL BIT4
#define INS_FUNC_N  BIT3
#define INS_FUNC_F  BIT2

#define INS_SET_CGRAM_DATA  (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5)
#define INS_SET_DDRAM_DATA  (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6)

/*instruction data template*/
#define INS_CLR_DATA_TMP        BIT0
#define INS_RTN_DATA_TMP        BIT1
#define INS_ENTRY_DATA_TMP      BIT2
#define INS_DSP_CTL_DATA_TMP    BIT3
#define INS_SHIFT_DATA_TMP      BIT4
#define INS_FUNC_DATA_TMP       BIT5
#define INS_SET_CGRAM_DATA_TMP  BIT6
#define INS_SET_DDRAM_DATA_TMP  BIT7


/*Masks*/
#define DATA_MASK (D4|D5|D6|D7)
#define CTL_MASK (RS|RW)
#define E_MASK E

#define INS_DSP_CTL_MASK    (INS_DSP_CTL_D|INS_DSP_CTL_C|INS_DSP_CTL_B)
#define INS_SHIFT_MASK      (INS_SHIFT_SC|INS_SHIFT_RL)

#define INS_FUNC_MASK       (INS_FUNC_DL|INS_FUNC_N|INS_FUNC_F)
#define INS_SET_CGRAM_MASK  (INS_SET_CGRAM_DATA)
#define INS_SET_DDRAM_MASK  (INS_SET_DDRAM_DATA)


/*Flags*/
#define BUSY_FLAG D7

#define INSTRUC_OP_W ((~RS_POS)&(~RW_POS))

#define MAX_INT_LEN 10




/******************************************************************************
 * VARIABLES
 *****************************************************************************/
typedef enum{
    INC,
    DEC
} EntryMode;

typedef enum{
    IR_WRITE,
    DR_WRITE,
    BF_READ,
    DR_READ
} RegSel;


/******************************************************************************
 * FUNCTION PROTOTYPES
 *****************************************************************************/
uint8_t lcd_get_cursor_x();
uint8_t lcd_get_cursor_y();



/** Initialization */

// Initialization of the LCD; set all pin directions,
// basic setup of the LCD, etc. (1 pt.)
void lcd_init (void);


/** Control functions */

// Enable (1) or disable (0) the display (i.e. hide all text) (0.5 pts.)
void lcd_enable (unsigned char on);

// Set the cursor to a certain x/y-position (0.5 pts.)
void lcd_cursorSet (unsigned char x, unsigned char y);

// Show (1) or hide (0) the cursor (0.5 pts.)
void lcd_cursorShow (unsigned char on);

// Blink (1) or don't blink (0) the cursor (0.5 pts.)
void lcd_cursorBlink (unsigned char on);


/** Data manipulation */

// Delete everything on the LCD (1 pt.)
void lcd_clear (void);

// Put a single character on the display at the cursor's current position (1 pt.)
void lcd_putChar (char character);

// Show a given string on the display. If the text is too long to display,
// don't show the rest (i.e. don't break into the next line) (1 pt.).
void lcd_putText (char * text);

// Show a given number at the cursor's current position.
// Note that this is a signed variable! (1 pt.)
void lcd_putNumber (int number);

/** Character Function */
void add_charactor(uint8_t addr, uint8_t* pattern, uint8_t len);


#endif /* LIBS_LCD_H_ */
