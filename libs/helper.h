/***************************************************************************//**
 * @file    helper.h
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    <date of creation>
 * @email   kaop@tf.uni-freiburg.de
 * @title   Exercise 1 - Display Interface
 * @brief   Delay for specific function is defined here for convenience
 *
 *
 * Here goes a detailed description if required.
 ******************************************************************************/

#ifndef LIBS_HELPER_H_
#define LIBS_HELPER_H_
#include <msp430g2553.h>

// avaliable option 1, 2 ,4 ,8 ,16 MHz
#define CPU_CLOCK 3 // 8MHZ

void E_delay();
void return_home_delay();
void buttons_latch_delay();
void buttons_clear_delay();

void std_delay_1ms();

void std_delay(unsigned int milliseconds);

#endif /* LIBS_HELPER_H_ */
