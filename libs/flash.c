/*
 * @file    flash.c
 * @author  Powen Kao
 * @matriculation-no 4653490
 * @date    16.06.2019
 * @email   kaop@tf.uni-freiburg.de
 * @brief   Exercise 4 - SPI
 */
#include "./flash.h"
void _flash_write_cmd(unsigned int cmd){
    uint8_t header[] ={
        cmd // command
    };
    CS_LOW;
    spi_write(sizeof(header), header);
    CS_HIGH;
}

unsigned int _flash_read_status(){
    uint8_t header[] ={
        CMD_READ_STATUS_REGISTER // command
    };
    CS_LOW;
    spi_write(sizeof(header), header);
    uint8_t reg = spi_read_byte();
    CS_HIGH;
    return reg;
}

void _flash_enable_write(){
    _flash_write_cmd(CMD_WRITE_ENABLE);
    
    // wait until enabled
    while (!(_flash_read_status() & WEL));
}
 


void flash_erase_sector(long int address){
    while (flash_busy()); // check if busy

    _flash_enable_write();

    uint8_t header[] ={
        CMD_SECTOR_ERASE, // command
        address >> 16, address >> 8, address // adddress
    };

    CS_LOW;
    spi_write(sizeof(header), header);
    CS_HIGH;
}


void flash_init(void){
    // don't need it here
}

// Read <length> bytes into <rxData> starting from address <address> (1 pt.)
void flash_read(long int address, unsigned char length, unsigned char * rxData){
    while (flash_busy()); // check if busy

    uint8_t header[] ={
        CMD_READ_DATA_BYTES, // command
        address >> 16, address >> 8, address // adddress
    };
    
    CS_LOW;
    spi_write(sizeof(header), header);
    spi_read(length, rxData);
    CS_HIGH;

}

// Write <length> bytes from <txData>, starting at address <address> (1 pt.)
void flash_write(long int address, unsigned char length, unsigned char * txData){
    while (flash_busy()); // check if busy

    _flash_enable_write();

    uint8_t header[] = {
        CMD_PAGE_PROGRAM, // command
        address >> 16, address >> 8, address // adddress
        // data to write
    };
    
    CS_LOW;
    spi_write(sizeof(header), header);
    spi_write(length, txData);
    CS_HIGH;
}

// Returns 1 if the FLASH is busy or 0 if not.
// Note: this is optional. You will probably need this, but you don't have to
// implement this if you solve it differently.
unsigned char flash_busy(void){
    //TODO: quick fix 
    return (_flash_read_status() & (WIP + WEL)) > 0;
}

